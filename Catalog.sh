#!/bin/bash

l_installer_url="https://updates.cdn-apple.com/2021/macos/041-7683-20210614-E610947E-C7CE-46EB-8860-D26D71F0D3EA/InstallMacOSX.dmg"
l_installer_name="Install Mac OS X Lion"
l_installer_version="10.7.5"

ml_installer_url="https://updates.cdn-apple.com/2021/macos/031-0627-20210614-90D11F33-1A65-42DD-BBEA-E1D9F43A6B3F/InstallMacOSX.dmg"
ml_installer_name="Install OS X Mountain Lion"
ml_installer_version="10.8.5"

y_installer_url="http://updates-http.cdn-apple.com/2019/cert/061-41343-20191023-02465f92-3ab5-4c92-bfe2-b725447a070d/InstallMacOSX.dmg"
y_installer_name="Install OS X Yosemite"
y_installer_version="10.10.5"

ec_installer_url="http://updates-http.cdn-apple.com/2019/cert/061-41424-20191024-218af9ec-cf50-4516-9011-228c78eda3d2/InstallMacOSX.dmg"
ec_installer_name="Install OS X El Capitan"
ec_installer_version="10.11.6"

s_installer_url="http://updates-http.cdn-apple.com/2019/cert/061-39476-20191023-48f365f4-0015-4c41-9f44-39d3d2aca067/InstallOS.dmg"
s_installer_name="Install macOS Sierra"
s_installer_version="10.12.6"

hs_installer_url="06/50/041-91758-A_M8T44LH2AW/b5r4og05fhbgatve4agwy4kgkzv07mdid9"
hs_installer_name="Install macOS High Sierra"
hs_installer_version="10.13.6"

m_installer_url="17/32/061-26589-A_8GJTCGY9PC/25fhcu905eta7wau7aoafu8rvdm7k1j4el"
m_installer_name="Install macOS Mojave"
m_installer_version="10.14.6"

c_installer_url="20/55/001-51042-A_2EJTJOSUC2/rsvf13iphg5lvcqcysqcarv8cvddq8igek"
c_installer_name="Install macOS Catalina"
c_installer_version="10.15.7"