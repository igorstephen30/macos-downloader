# macOS Downloader
macOS Downloader is an [open source](#license) command line tool for downloading macOS installers and beta updates

## Direct Download Links
You can download Mac OS X 10.7.5 Lion, OS X 10.8.5 Mountain Lion, OS X 10.10.5 Yosemite, OS X 10.11.6 El Capitan, and macOS 10.12.6 Sierra, directly from Apple using these links:
- [10.7.5 Lion](https://updates.cdn-apple.com/2021/macos/041-7683-20210614-E610947E-C7CE-46EB-8860-D26D71F0D3EA/InstallMacOSX.dmg)
- [10.8.5 Mountain Lion](https://updates.cdn-apple.com/2021/macos/031-0627-20210614-90D11F33-1A65-42DD-BBEA-E1D9F43A6B3F/InstallMacOSX.dmg)
- [10.10.5 Yosemite](http://updates-http.cdn-apple.com/2019/cert/061-41343-20191023-02465f92-3ab5-4c92-bfe2-b725447a070d/InstallMacOSX.dmg)
- [10.11.6 El Capitan](http://updates-http.cdn-apple.com/2019/cert/061-41424-20191024-218af9ec-cf50-4516-9011-228c78eda3d2/InstallMacOSX.dmg)
- [10.12.6 Sierra](http://updates-http.cdn-apple.com/2019/cert/061-39476-20191023-48f365f4-0015-4c41-9f44-39d3d2aca067/InstallOS.dmg)

Links for macOS 11 Big Sur and macOS 12 Monterey would not be up to date, as Apple is still releasing updates to these versions. If you run macOS Downloader, you will get the latest version. You can also run the Search Catalog script to search Apple's catalog for the latest versions.

Additionally, the Internet Archive has a copy of the OS X 10.9.5 Mavericks installer app and if you have a Mac that doesn't support Internet Recovery, you can also download a copy of Mac OS X 10.7.5 Lion, OS X 10.8.5 Mountain Lion, OS X 10.9.5 Mavericks, and OS X 10.10.5 Yosemite to macOS 12.2 Monterey Beta as disc images you can put on a USB from these links:
- [10.9.5 Mavericks](https://archive.org/details/os-x-mavericks-installer-app)
- [10.7.5 Lion disc image](https://archive.org/details/mac-os-x-lion-10.7.5)
- [10.8.5 Mountain Lion disc image](https://archive.org/details/install-esd-os-x-mountain-lion-10.8.5)
- [10.9.5 Mavericks disc image](https://archive.org/details/os-x-mavericks_202202)
- [10.10.5 Yosemite to 12 Monterey disc images](https://archive.org/details/macos-collection)

If you have a Mac that doesn't support Internet Recovery, and you wanted to directly install the latest version of macOS supported by macOS Patcher, you can download a patched copy of macOS 10.15.7 Catalina as a disc image you can put on a USB from this link:
- [10.15.7 Catalina Patched disc image](https://archive.org/details/apple-macos-catalina-patched-iso)


## Creating Installer Media
If you download a macOS installer for macOS 10.15 Catalina or newer on Mac OS X 10.7 Lion or OS X 10.8 Mountain Lion, you will not be able to use the createinstallmedia tool, which requires OS X 10.9 Mavericks or newer. If this is an issue for you, you should try using my [openinstallmedia](https://gitlab.com/julianfairfax/openinstallmedia) tool, which is open source install media creation tool for macOS.

## Contributors
I'd like to the thank the following people, and many others, for their research, help, and inspiration.
- [pbzx](https://github.com/nrosenstein-stuff/pbzx) compiled by [dosdude1](https://github.com/dosdude1)
- [curl](https://github.com/curl/curl) compiled by [SiliconExarch](https://github.com/SiliconExarch)

## Supported Versions of macOS
- 10.7 Lion
- 10.8 Mountain Lion
- 10.9 Mavericks
- 10.10 Yosemite
- 10.11 El Capitan
- 10.12 Sierra
- 10.13 High Sierra
- 10.14 Mojave
- 10.15 Catalina
- 11 Big Sur
- 12 Monterey

## Downloadable Versions of macOS
- 10.7 Lion
- 10.8 Mountain Lion
- 10.10 Yosemite
- 10.11 El Capitan
- 10.12 Sierra
- 10.13 High Sierra
- 10.14 Mojave
- 10.15 Catalina
- 11 Big Sur
- 12 Monterey

## Usage

### Step 1

Download the latest version of macOS Downloader from the GitHub releases page.

You can also download it from the Homebrew repository that is now available [here](https://gitlab.com/julianfairfax/package-repo).

### Step 2

Unzip the download and open Terminal. Type chmod +x and drag the script file to Terminal, then hit enter. Then drag the script file to Terminal, and hit enter.

## License
The following files and folders were created by me and are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- Catalog.sh
- macOS Downloader.sh
- Search Catalog.sh

The following files and folders were created by other developers and are licensed under their licenses:
- resources/cacert.pem, complied by SiliconExarch, from curl
- resources/curl, compiled by SiliconExarch, from curl
- resources/pbzx, compiled by dosdude1, from NiklasRosenstein
