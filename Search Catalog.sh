for entry in $(curl -L -s https://swscan.apple.com/content/catalogs/others/index-10.16seed-10.16-10.15-10.14-10.13-10.12-10.11-10.10-10.9-mountainlion-lion-snowleopard-leopard.merged-1.sucatalog | grep "InstallAssistant.pkg<" | sed 's/.*<string>//' | sed 's/\/InstallAssistant.*//'); do 
    info=$(curl -L -s "$entry/Info.plist")
    version="$(echo "$info" | grep -A1 OSVersion | grep string | sed 's/.*<string>//' | sed 's/<\/string>//')"
    isseed="$(echo "$info" | grep -A1 IsSeed | grep string | sed 's/.*<string>//' | sed 's/<\/string>//')"

    if [[ "$version" == 11* ]]; then 
        if [[ "$isseed" == "NO" ]]; then 
            bs_installer_urls="$bs_installer_urls
$entry/InstallAssistant.pkg"
			bs_installer_versions="$bs_installer_versions
$version"
    	else
    	    bs_beta_installer_urls="$bs_beta_installer_urls
$entry/InstallAssistant.pkg"
			bs_beta_installer_versions="$bs_beta_installer_versions
$version"
        fi
    fi

    if [[ "$version" == 12* ]]; then 
        if [[ "$isseed" == "NO" ]]; then 
            mo_installer_urls="$mo_installer_urls
$entry/InstallAssistant.pkg"
			mo_installer_versions="$mo_installer_versions
$version"
    	else
    	    mo_beta_installer_urls="$mo_beta_installer_urls
$entry/InstallAssistant.pkg"
			mo_beta_installer_versions="$mo_beta_installer_versions
$version"
        fi
    fi
done

bs_installer_url="$(echo "$bs_installer_urls" | tail -1)"
bs_installer_version="$(echo "$bs_installer_versions" | tail -1)"
bs_beta_installer_url="$(echo "$bs_beta_installer_urls" | tail -1)"
bs_beta_installer_version="$(echo "$bs_beta_installer_versions" | tail -1)"

echo "- [$bs_installer_version Big Sur]($bs_installer_url)"

if [[ (( "$bs_beta_installer_version" > "$bs_installer_version" )) ]]; then
	echo "- [$bs_beta_installer_version Big Sur Beta]($bs_beta_installer_url)"
fi

mo_installer_url="$(echo "$mo_installer_urls" | tail -1)"
mo_installer_version="$(echo "$mo_installer_versions" | tail -1)"
mo_beta_installer_url="$(echo "$mo_beta_installer_urls" | tail -1)"
mo_beta_installer_version="$(echo "$mo_beta_installer_versions" | tail -1)"

echo "- [$mo_installer_version Monterey]($mo_installer_url)"

if [[ (( "$mo_beta_installer_version" > "$mo_installer_version" )) ]]; then
	echo "- [$mo_beta_installer_version Monterey Beta]($mo_beta_installer_url)"
fi